from pickle import dump

from lxml import etree

from utils import to_date


def mine_first_posts(posts, out):
    users = {}
    for _, elem in etree.iterparse(posts, events=('start',)):
        if elem.tag == 'row':
            created = to_date(elem.attrib['CreationDate'])
            user_id = int(elem.attrib['OwnerUserId'])
            prev = users.get(user_id, None)
            if prev is None or prev > created:
                users[user_id] = created
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]

    with open(out, "wb") as f:
        dump(users, f)


def mine_reputation(ufn, out):
    users = {}
    for _, elem in etree.iterparse(ufn, events=('start',)):
        if elem.tag == 'row':
            rep = int(elem.attrib['Reputation'])
            user_id = int(elem.attrib['Id'])
            users[user_id] = rep
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]

    with open(out, "wb") as f:
        dump(users, f)
