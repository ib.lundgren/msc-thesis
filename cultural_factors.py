import csv

cultural_factors = {}

countries = set()

with open('world_cultural_factors.csv') as f:
    reader = csv.reader(f)

    cols = []
    for i, row in enumerate(reader):
        if i == 0:
            cols.extend(row[1:])
            for c in row[1:]:
                cultural_factors[c] = {}
        else:
            country = row[0].title()
            if country == 'Russia':
                country = 'Russian Federation'
            countries.add(country)
            for c, v in zip(cols, row[1:]):
                if v:
                    cultural_factors[c][country] = float(v)
