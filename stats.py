from collections import defaultdict
from os.path import basename, dirname, exists, join
from lxml import etree

from manager import Manager
from numpy import average

from abnormal_return import AbnormalReturn
from insert_country import guess_country
from utils import parse_pn, load_quality, to_day


manager = Manager()

def get_for_users(elem):
    loc = elem.attrib.get('Location', None)
    if loc is not None:
        return guess_country(loc)


def get_country(elem):
    return elem.attrib.get('Country', None)


def distribution(fn, out, get_country, filter_=lambda x: True):
    users = defaultdict(int)
    total = 0

    cfn = join(dirname(fn), 'CU' + basename(fn))
    if exists(cfn):
        for _, elem in etree.iterparse(cfn, events=('start',)):
            if elem.tag == 'row':
                country = get_country(elem)
                if country is not None and filter_(elem):
                    users[country] += 1
            elem.clear()
            while elem.getprevious() is not None:
                del elem.getparent()[0]

        for _, elem in etree.iterparse(fn, events=('start',)):
            if elem.tag == 'row' and filter_(elem):
                total += 1
            elem.clear()
            while elem.getprevious() is not None:
                del elem.getparent()[0]
    else:
        for _, elem in etree.iterparse(fn, events=('start',)):
            if elem.tag == 'row':
                country = get_country(elem)
                if country is not None:
                    users[country] += 1
                total += 1
            elem.clear()
            while elem.getprevious() is not None:
                del elem.getparent()[0]

    with open(out, "w") as f:
        f.write('total\t%d\n' % total)
        f.write('country\t%d\n' % sum(users.values()))
        for c in users:
            f.write('%s\t%d\n' % (c, users[c]))


@manager.command
def user_distribution(ufn, out='statistics'):
    distribution(ufn, join(out, 'user_distribution'), get_for_users)


@manager.command
def post_distribution(pfn, out='statistics'):
    distribution(pfn, join(out, 'post_distribution'), get_country)


@manager.command
def vote_distribution(vfn, out='statistics'):
    distribution(vfn, join(out, 'vote_distribution'), get_country)
    upvote = lambda x: x.attrib['VoteTypeId'] == '2'
    distribution(vfn, join(out, 'upvote_distribution'), get_country, upvote)
    downvote = lambda x: x.attrib['VoteTypeId'] == '3'
    distribution(vfn, join(out, 'downvote_distribution'), get_country, downvote)


@manager.command
def badge_distribution(bfn, out='statistics'):
    distribution(bfn, join(out, 'badge_distribution'), get_country)


@manager.command
def significant_events(vfn, out='statistics'):
    posts = defaultdict(int)
    with open(vfn) as f:
        for line in f:
            _, _, pn = line.partition('\t')
            p, n = parse_pn(pn)
            q = float(p) / (p + n)
            for b, score in AbnormalReturn.bins:
                if score[0] <= q <= score[1]:
                    posts[b] += 1

    with open(join(out, 'significant_events'), 'w') as f:
        for b in posts:
            f.write('%s\t%d\n' % (b, posts[b]))


def significant(fn, out, filter_=lambda x: True):
    totals = defaultdict(int)
    with open(fn) as f:
        for line in f:
            cu, _, badges = line.partition('\t')
            country = eval(cu)[0]
            badges = eval(badges)
            for _, b in badges:
                if filter_(b):
                    totals[country] += 1

    with open(out, 'w') as f:
        f.write('total\t%d\n' % sum(totals.values()))
        for c in totals:
            f.write('%s\t%d\n' % (c, totals[c]))


@manager.command
def significant_posts(fn, qfn, out='statistics'):
    significant(fn, join(out, 'significant_posts'))
    quality, _, _ = load_quality(qfn)
    for b, (_, score) in zip(AbnormalReturn.fn_bins, AbnormalReturn.bins):
        filter_ = lambda x: score[0] <= quality[int(x)] <= score[1]
        significant(fn, join(out, 'significant_posts_' + b), filter_)


@manager.command
def significant_badges(fn, out='statistics'):
    significant(fn, join(out, 'significant_badges'))


def frequency_distribution(fn, out, filter_=lambda x: True):
    stats = defaultdict(lambda: defaultdict(int))
    for _, elem in etree.iterparse(fn, events=('start',)):
        if elem.tag == 'row':
            country = get_country(elem)
            day = elem.attrib.get('CreationDate', elem.attrib.get('Date', None))
            day = to_day(day)
            yw = day.year, day.isocalendar()[1]
            if country is not None and filter_(elem):
                stats[yw][country] += 1
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]

    with open(out, 'w') as f:
        for yw in sorted(stats):
            f.write('%r\t%r\n' % (yw, stats[yw].items()))


@manager.command
def post_frequency(pfn, out='statistics'):
    frequency_distribution(pfn, join(out, 'post_frequency'))


@manager.command
def vote_frequency(vfn, out='statistics'):
    frequency_distribution(vfn, join(out, 'vote_frequency'))
    upvote = lambda x: x.attrib['VoteTypeId'] == '2'
    frequency_distribution(vfn, join(out, 'upvote_frequency'), upvote)
    downvote = lambda x: x.attrib['VoteTypeId'] == '3'
    frequency_distribution(vfn, join(out, 'downvote_frequency'), downvote)


@manager.command
def quality_distribution(pfn, qfn, out='statistics'):
    quality, _, _ = load_quality(qfn)
    stats = defaultdict(lambda: defaultdict(list))
    for _, elem in etree.iterparse(pfn, events=('start',)):
        if elem.tag == 'row':
            country = get_country(elem)
            pid = int(elem.attrib['Id'])
            day = elem.attrib.get('CreationDate', elem.attrib.get('Date', None))
            day = to_day(day)
            yw = day.year, day.isocalendar()[1]
            q = quality.get(pid, None)
            if country is not None and q is not None:
                stats[yw][country].append(q)
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]

    for yw in stats:
        for c in stats[yw]:
            stats[yw][c] = average(stats[yw][c])

    with open(join(out, 'quality_distribution'), 'w') as f:
        for yw in sorted(stats):
            f.write('%r\t%r\n' % (yw, stats[yw].items()))


if __name__ == "__main__":
    manager.main()
