from random import sample
from lxml import etree
from sys import argv

from readchar import readchar

from insert_country import guess_country

if __name__ == "__main__":
    users = []
    for _, elem in etree.iterparse(argv[1], events=('start',)):
        if elem.tag == 'row':
            loc = elem.attrib.get('Location', None)
            if loc is not None:
                country = guess_country(loc)
                if country is not None:
                    users.append((loc, country))
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]

    correct = 0
    total = 0

    for i, (l, c) in enumerate(sample(users, 200)):
        #y = raw_input('%d) %s == %s?: ' % (i, l, c))
        print '%d) %s == %s?: ' % (i, l, c),
        y = readchar()
        if y == 'y':
            correct += 1
        else:
            y = 'n'
        total += 1
        print y

    print "Correct", correct
    print "Total", total
    print "Ratio", float(correct) / total
