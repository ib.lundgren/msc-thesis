from mrjob.job import MRJob

from utils import get_postid_and_vote_type


class CountVotes(MRJob):

    def mapper(self, _, line):
        v, post_id, vote_type = get_postid_and_vote_type(line)
        if not post_id:
            return

        vote = (post_id, v['PostCreationDate'])

        if vote_type == 2:
            yield vote, (1, 0)
        elif vote_type == 3:
            yield vote, (0, 1)

    def reducer(self, vote, votes):
        p, n = 0, 0
        for up, down in votes:
            p += up
            n += down
        if (p + n) == 0:
            return

        if self.threshold(p, n):
            yield vote[0], (p, n, vote[1])

    def threshold(self, p, n):
        raise NotImplementedError()


class CountAll(CountVotes):

    def threshold(self, p, n):
        return True


class CountAbove(CountVotes):

    def threshold(self, p, n):
        return (p + (2.5 * n)) >= 5


if __name__ == "__main__":
    CountVotes.run()
