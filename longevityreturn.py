import gzip

from isoweek import Week

from abnormal_return import AbnormalReturn
from utils import to_day


class LongevityReturn(AbnormalReturn):

    strategy = "Longevity"

    def init_binner(self, lfn):
        self.longevity = {}
        f = gzip.open(lfn)
        for line in f:
            if len(line) > 120:
                continue
            user_id, _, l = line.partition('\t')
            self.longevity[int(user_id)] = to_day(l)
        f.close()

    def binner(self, user_id, year, week):
        created = self.longevity.get(user_id, None)
        if created is None:
            return None
        event_day = Week(year, week).monday()
        l = (event_day - created).days / 7

        if 0 <= l <= 4:
            return '0 - 4'
        if 4 < l <= 12:
            return '4 - 12'
        if 12 < l <= 52:
            return '12 - 52'
        elif l > 52:
            return 'Over 52'
        else:
            raise ValueError("%s%s%s" % (user_id, year, week))
            return None


if __name__ == "__main__":
    LongevityReturn.run()
