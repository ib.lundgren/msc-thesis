from collections import defaultdict
from functools import partial
from os.path import join, exists
from cPickle import load

from isoweek import Week

from utils import user_history

power_distance = {
    "Argentina":49,
    "Australia":36,
    "Austria":11,
    "Belgium":65,
    "Brazil":69,
    "Canada":39,
    "China":80,
    "Colombia":67,
    "Costa Rica":35,
    "Denmark":18,
    "Ecuador":78,
    "Egypt":80,
    "El Salvador":66,
    "Finland":33,
    "France":68,
    "Germany":35,
    "Greece":60,
    "Guatemala":95,
    "Hong Kong":68,
    "India":77,
    "Indonesia":78,
    "Iran":58,
    "Iraq":80,
    "Ireland":28,
    "Israel":13,
    "Italy":50,
    "Jamaica":45,
    "Japan":54,
    "Libya":80,
    "Malaysia":104,
    "Mexico":81,
    "Netherlands":38,
    "New Zealand":22,
    "Nigeria":77,
    "Norway":31,
    "Pakistan":55,
    "Panama":95,
    "Peru":64,
    "Philippines":94,
    "Poland":68,
    "Portugal":63,
    "Russian Federation":93,
    "Saudi Arabia":80,
    "Sierra Leon":77,
    "Singapore":49,
    "South Korea":60,
    "Spain":57,
    "Sweden":31,
    "Switzerland":34,
    "Taiwan":58,
    "Thailand":64,
    "Turkey":66,
    "United Arab Emirates":80,
    "United Kingdom":35,
    "United States":40,
    "Uruguay":61,
    "Venezuela":81,
}


def power_distance_class(country):
    pd = power_distance.get(country, None)
    if pd is None:
        return None
    if pd <= 40:
        return 'PD-0-40'
    if 40 < pd < 80:
        return 'PD-41-79'
    if pd >= 80:
        return 'PD-Over80'
    raise ValueError(pd)


def binning_strategies(db_dir, badges=False):
    prefix = 'badges_' if badges else ''
    ufn = join(db_dir, 'rep_db')
    lfn = join(db_dir, 'longevity_db')
    afn = join(db_dir, prefix + 'activity')
    qfn = join(db_dir, prefix + 'quality')
    q2fn = join(db_dir, prefix + 'quality2')
    ffn = join(db_dir, prefix + 'feedback')

    for fn in (ufn, lfn, afn, qfn, q2fn, ffn):
        if not exists(fn):
            raise ValueError("Missing file %s, check pre required steps." % fn)

    return [
        ('Activity', by_activity(afn)),
       #('Reputation', by_reputation(ufn)),
       #('Quality', by_quality(qfn)),
       #('Quality2', by_quality2(q2fn)),
       #('Longevity', by_longevity(lfn)),
       #('Feedback', by_feedback(ffn)),
    ]


def by_reputation(ufn):
    users = load(open(ufn))
    def bin_by_reputation(country, user_id, event_id, score, year, week, b, w, m, l):
        rep = users.get(user_id, None)
        if rep is None:
            return None
        if rep <= 100:
            return '0 - 100'
        if 100 < rep <= 1000:
            return '100 - 1000'
        if 1000 < rep <= 10000:
            return '1000 - 10k'
        if rep > 10000:
            return 'Over 10k'
        raise ValueError(rep)
    return bin_by_reputation


def by_activity(afn):
    activity = user_history(afn)
    def bin_by_activity(country, user_id, event_id, score, year, week, b, w, m, l):
        a = activity.get(hash((user_id, year, week)), None)
        if a is None:
            return None
        if a == 0:
            return '0.00'
    #if 0 < a <= 0.07:
    #    return '0.00 - 0.07'
    #if 0.07 < a <= 0.14:
    #    return '0.07 - 0.14'
    #if 0.14 < a <= 0.35:
    #    return '0.14 - 0.35'
    #if a > 0.35:
    #    return 'Over 0.35'
        if 0 < a <= 0.49:
            return '0.00 - 0.49'
        if 0.49 < a <= 1.00:
            return '0.49 - 1.00'
        if 1.00 < a <= 2.00:
            return '1.00 - 2.00'
        if a > 2.00:
            return 'Over 2.00'
        raise ValueError(a)
    return bin_by_activity


def by_quality(qfn):
    quality = user_history(qfn)
    def bin_by_quality(country, user_id, event_id, score, year, week, b, w, m, l):
        q = quality.get(hash((user_id, year, week)), None)
        if q is None:
            return None
        if q == 0:
            return '0.00'
        if 0 < q < 0.5:
            return '0.00 - 0.49'
        if 0.5 <= q <= 0.75:
            return '0.50 - 0.74'
        if 0.75 < q < 1.0:
            return '0.75 - 0.99'
        if q >= 1.0:
            return '1.00'
        if q == -1:
            return None
        if q == -2:
            return None
        raise ValueError(q)
    return bin_by_quality


def by_quality2(q2fn):
    quality2 = user_history(q2fn)
    def bin_by_quality2(country, user_id, event_id, score, year, week, b, w, m, l):
        q = quality2.get(hash((user_id, year, week)), None)
        if q is None:
            return None
        if q == 0:
            return '0.00'
        if 0 < q < 0.5:
            return '0.00 - 0.49'
        if 0 > q > -0.5:
            return '-0.49 - 0.00'
        if -0.5 >= q > -1.00:
            return '-0.99 - -0.50'
        if 0.50 <= q < 1.0:
            return '0.50 - 0.99'
        if q == 1.0:
            return '1.00'
        if q == -1:
            return '-1.00'
        if q == -2:
            return None
        raise ValueError(q)
    return bin_by_quality2


def by_longevity(lfn):
    longevity = load(open(lfn))
    def bin_by_longevity(country, user_id, event_id, score, year, week, b, w, m, l):
        created = longevity.get(user_id, None)
        if created is None:
            return None
        created = created.date()
        event_date = Week(year, week).monday()
        lifetime = (event_date - created).days / 7
        if lifetime <= 4:
            return '0 - 4'
        if 4 < lifetime <= 12:
            return '4 - 12'
        if 12 < lifetime <= 52:
            return '12 - 52'
        if lifetime > 52:
            return 'Over 52'
        raise ValueError(lifetime)
    return bin_by_longevity


def by_feedback(ffn):
    feedback = user_history(ffn)
    def bin_by_feedback(country, user_id, event_id, score, year, week, b, w, m, l):
        q = feedback.get(hash((user_id, year, week)), None)
        if q is None:
            return None
        if q == 0:
            return '0.00'
        if 0 < q < 0.5:
            return '0.00 - 0.49'
        if 0.5 <= q < 1.00:
            return '0.50 - 0.99'
        if q >= 1.0:
            return '1.00'
        if q == -1:
            return None
        if q == -2:
            return None
        raise ValueError(q)
    return bin_by_feedback


class Events(object):

    power_distances = ['PD-0-40', 'PD-41-79', 'PD-Over80']

    bins = (
        ('p == 0', (0.0, 0.00)),
        ('0 < p < 0.5', (0.01, 0.49)),
        ('0.5 <= p < 1.0', (0.50, 0.99)),
        ('p == 1.0', (1.00, 1.00)),
        ('all', (0.00, 1.00)),
        ('control', (-2.00, -2.00)),
    )

    windows = ('Before', 'Week', 'Month', '2 Months')

    def __init__(self, metric, binner):
        self.data = defaultdict(
            partial(defaultdict,
                    partial(defaultdict,
                            partial(defaultdict, list))))
        self.global_data = defaultdict(
                partial(defaultdict,
                        partial(defaultdict, list)))
        self.pd_data = defaultdict(
            partial(defaultdict,
                    partial(defaultdict,
                            partial(defaultdict, list))))
        self.metric = metric
        self.binner = binner
        self.labels = set()
        self._countries = set()

    @property
    def countries(self):
        pd_countries = filter(lambda c: c in power_distance, self._countries)
        return sorted(pd_countries, key=lambda c: power_distance[c])

    def add_events(self, measurements, ge=0):
        for meta, change in measurements:
            self._countries.add(meta[0])
            for i, (name, score) in enumerate(self.bins):
                bin_ = self.binner(*(meta + change))
                if bin_ is None:
                    continue
                self.labels.add(bin_)
                if score[0] <= meta[3] <= score[1]:
                    for j, window in enumerate(self.windows):
                        if change[j] >= ge:
                            self.data[name][window][meta[0]][bin_].append(change[j])
                            self.global_data[name][window][bin_].append(change[j])
                        else:
                            self.data[name][window][meta[0]][bin_].append(None)
                            self.global_data[name][window][bin_].append(None)
                           #pd = power_distance_class(meta[0])
                           #if pd is not None:
                           #    self.pd_data[name][window][pd][bin_].append(change[j])

        for q, _ in self.bins:
            for w in self.windows:
                for c in self.countries:
                    for b in self.labels:
                        if b not in self.data[q][w][c]:
                            self.data[q][w][c][b] = None
                for b in self.labels:
                    if b not in self.global_data[q][w]:
                        self.global_data[q][w][b] = None
