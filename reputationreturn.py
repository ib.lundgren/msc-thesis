import gzip

from abnormal_return import AbnormalReturn


class ReputationReturn(AbnormalReturn):

    strategy = "Reputation"

    def init_binner(self, ufn):
        f = gzip.open(ufn)
        self.users = {}
        for line in f:
            if len(line) > 100:
                continue
            user_id, _, rep = line.partition('\t')
            self.users[int(user_id)] = int(rep)
        f.close()

    def binner(self, user_id, year, week):
        rep = self.users.get(user_id, None)
        if rep is None:
            return None
        if rep <= 100:
            return '0 - 100'
        if 100 < rep <= 1000:
            return '100 - 1000'
        if 1000 < rep <= 10000:
            return '1000 - 10k'
        if rep > 10000:
            return 'Over 10k'
        raise ValueError(rep)


if __name__ == "__main__":
    ReputationReturn.run()
