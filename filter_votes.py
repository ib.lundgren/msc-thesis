from os.path import basename, dirname, join

from lxml import etree


def filter_votes(pfn, vfn, prefix='F'):
    pids = set()
    with open(pfn) as f:
        for line in f:
            pid, _, _ = line.partition('\t')
            pids.add(int(pid))

    with open(join(dirname(vfn), prefix + basename(vfn)), 'w') as f:
        f.write('<?xml version="1.0" encoding="utf-8"?>\n')
        f.write('<votes>\n')
        for _, elem in etree.iterparse(vfn, events=('start',)):
            if elem.tag == 'row':
                post_id = int(elem.attrib['PostId'])
                if post_id in pids:
                    f.write(etree.tostring(elem).strip())
                    f.write('\n')
            elem.clear()
            while elem.getprevious() is not None:
                del elem.getparent()[0]
        f.write('</votes>\n')
