from os.path import basename, dirname, join

from lxml import etree

def insert_post_data(pfn, vfn):
    posts = {}
    print "Reading in countries, user ids and creation dates."
    for _, elem in etree.iterparse(pfn, events=('start',)):
        if elem.tag == 'row':
            post_id = int(elem.attrib['Id'])
            created = str(elem.attrib['CreationDate'])
            country = str(elem.attrib['Country'])
            user_id = str(elem.attrib['OwnerUserId'])
            posts[post_id] = (country, user_id, created)
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]


    print "Inserting data in votes."
    with open(join(dirname(vfn), 'CU' + basename(vfn)), 'w') as f:
        f.write('<?xml version="1.0" encoding="utf-8"?>\n')
        f.write('<votes>\n')
        for _, elem in etree.iterparse(vfn, events=('start',)):
            if elem.tag == 'row':
                post_id = int(elem.attrib['PostId'])
                country, user_id, created = posts.get(post_id, (None, None, None))
                if country is not None:
                    elem.attrib['PostCreationDate'] = created
                    elem.attrib['OwnerUserId'] = user_id
                    elem.attrib['Country'] = country
                    f.write(etree.tostring(elem).strip())
                    f.write('\n')
            elem.clear()
            while elem.getprevious() is not None:
                del elem.getparent()[0]

        f.write('</votes>')


def insert_user_data(pfn, bfn):
    users = {}
    print "Reading in countries and user ids."
    for _, elem in etree.iterparse(pfn, events=('start',)):
        if elem.tag == 'row':
            country = str(elem.attrib['Country'])
            user_id = int(elem.attrib['OwnerUserId'])
            users[user_id] = country
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]

    print "Inserting countries in badges."
    with open(join(dirname(bfn), 'CU' + basename(bfn)), 'w') as f:
        f.write('<?xml version="1.0" encoding="utf-8"?>\n')
        f.write('<badges>\n')
        for _, elem in etree.iterparse(bfn, events=('start',)):
            if elem.tag == 'row':
                user_id = int(elem.attrib['UserId'])
                country = users.get(user_id, None)
                if country is not None:
                    elem.attrib['Country'] = country
                    f.write(etree.tostring(elem).strip())
                    f.write('\n')
            elem.clear()
            while elem.getprevious() is not None:
                del elem.getparent()[0]

        f.write('</badges>')
