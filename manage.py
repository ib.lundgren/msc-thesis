from collections import defaultdict
from glob import glob
from multiprocessing import Pool, cpu_count
from os import rename, environ
from os.path import basename, join, exists

from boto import connect_s3
from manager import Manager

from activityreturn import ActivityReturn
from feedbackreturn import FeedbackReturn
from longevityreturn import LongevityReturn
from qualityreturn import QualityReturn
from reputationreturn import ReputationReturn

from abnormal_returns import AbnormalReturns
from concentrated_arrivals import ConcentratedArrivals
from count_votes import CountAll, CountAbove
from filter_votes import filter_votes as fv
from find_users import find_controls as fc
from find_users import find_users as fu
from find_users import find_badges_users as fbu
from insert_country import insert_country
from insert_post_data import insert_post_data as ipd
from insert_post_data import insert_user_data as iud
from measure_change import change_metrics
from measure_change import voting_metrics
from user_mining import mine_first_posts as mfp
from user_mining import mine_reputation as mr
from utils import run_job

manager = Manager()


@manager.arg('trim', required=False)
@manager.command
def insert_countries(posts, users, trim=True):
    insert_country(posts, users, trim)


@manager.command
def insert_post_data(posts, votes):
    ipd(posts, votes)


@manager.command
def insert_user_data(posts, badges):
    iud(posts, badges)


@manager.command
def concentrated_arrivals(wd, votes):
    run_job(ConcentratedArrivals, [votes],
            join(wd, "concentrated_pid_dates"))


@manager.arg('prefix', required=False)
@manager.command
def filter_votes(pids, votes, prefix='C'):
    fv(pids, votes, prefix=prefix)


@manager.command
def votes_per_pid(wd, votes, concentrated_votes):
    pool = Pool(processes=cpu_count())
    pool.apply_async(run_job, (CountAll, [votes],
                               join(wd, "votes_per_pid")))
    pool.apply_async(run_job, (CountAbove, [concentrated_votes],
                               join(wd, "votes_per_pid_above")))
    pool.close()
    pool.join()


@manager.arg('future', required=False)
@manager.arg('badges', required=False)
@manager.command
def find_subjects(wd, posts, future=False, badges=False):
    if badges:
        fbu(posts, join(wd, 'subjects'), future=future)
    else:
        apids = join(wd, 'votes_per_pid_above')
        fu(posts, apids, join(wd, 'subjects'), future=future)


@manager.command
def find_controls(wd):
    subjects_all = join(wd, 'subjects_all')
    fc(subjects_all, join(wd, 'controls'))


def _measure(metrics, fn, o):
    cargs = [
        '--subjects',
        join(o, 'subjects'),
        '--controls',
        join(o, 'controls'),
        '--votes',
        join(o, 'votes_per_pid'),
        fn
    ]
    pool = Pool(processes=cpu_count())

    print cargs
    for m, MC in metrics:
        pool.apply_async(run_job, (MC, cargs, join(o, m)))

    cargs = ['--control', 'True'] + cargs
    print cargs
    for m, MC in metrics:
        pool.apply_async(run_job, (MC, cargs, join(o, "c" + m)))
    pool.close()
    pool.join()


@manager.command
def measure_change(wd, pfn):
    _measure(change_metrics, pfn, wd)


@manager.command
def measure_voting(wd, vfn):
    _measure(voting_metrics, vfn, wd)


@manager.command
def merge_measurements(wd):
    # Writing concurrently is not safe, just merge after instead
    for m, _ in change_metrics + voting_metrics:
        try:
            with open(join(wd, 'c' + m), 'a') as of:
                with open(join(wd, m)) as f:
                    for line in f:
                        of.write(line)
            rename(join(wd, 'c' + m), join(wd, m))
        except IOError as e:
            print e


@manager.arg('bucket', required=False)
@manager.command
def sync_s3(wd, bucket='sothesis'):
    conn = connect_s3(environ['S3_KEY'], environ['S3_SECRET'])
    _bucket = conn.get_bucket(bucket)
    rwd = wd
    wd = basename(wd)

    returns = set()
    for f in _bucket.get_all_keys(prefix=join(wd, 'AR-')):
        r = f.key[f.key.find('/') + 1:f.key.rfind('/')]
        returns.add(r)

    for r in returns:
        print "Syncing", r
        rfn = join(wd, r)
        if exists(join(rwd, r)):
            print "Ignoring", rfn, "since it exists already"
        else:
            print "Storing", join(rwd, r)
            with open(join(rwd, r), 'w') as of:
                for f in _bucket.get_all_keys(prefix=rfn):
                    if 'part' in f.key:
                        for line in f:
                            of.write(line)


def binning_strategies(db_dir, emr=False):
    ufn = join(db_dir, 'reputation.tar.gz')
    lfn = join(db_dir, 'longevity.tar.gz')
    afn = join(db_dir, 'activity.tar.gz')
    qfn = join(db_dir, 'quality.tar.gz')
    ffn = join(db_dir, 'feedback.tar.gz')

    for fn in (ufn, lfn, afn, qfn, ffn):
        if not emr and not exists(fn):
            raise ValueError("Missing file %s, check pre required steps." % fn)

    return [
        (ActivityReturn, afn),
        (ReputationReturn, ufn),
        (QualityReturn, qfn),
        (LongevityReturn, lfn),
        (FeedbackReturn, ffn),
    ]

@manager.arg('badges', required=False)
@manager.arg('emr', required=False)
@manager.command
def abnormal_return(wd, mfn, badges=False, emr=False):
    if mfn == 'all':
        #mfns = ['activity', 'quality', 'upvotes', 'voting_activity', 'downvotes']
        mfns = ['activity', 'quality', 'upvotes', 'downvotes']
    else:
        mfns = [mfn]

    #pool = Pool(processes=cpu_count() - 1)
    if emr:
        # All work will be on EMR, spawn all jobs we need.
        pool = Pool(processes=28)
    else:
        pool = Pool(processes=cpu_count() - 1)

    if emr:
        conn = connect_s3(environ['S3_KEY'], environ['S3_SECRET'])

    for mfn in mfns:
        mfn = join(wd, mfn + ".tar.gz")
        if emr:
            bucket_name = mfn[5: mfn[5:].find('/') + 5]
            bucket = conn.get_bucket(bucket_name)
        for MC, data_file in binning_strategies(wd, emr=emr):
            cargs = ['--binner_datafile', data_file]
            fn = "AR-" + basename(mfn).capitalize() + "-" + MC.strategy
            fn = fn.replace('.tar.gz', '')
            if emr:
                prefix = join(wd, fn)[6 + len(bucket_name):]
                for f in bucket.get_all_keys(prefix=prefix):
                    print "Deleting", f
                    #f.delete()

            if badges:
                cargs.extend(('--badges', 'True'))
            cargs.append(mfn)
            pool.apply_async(run_job, (MC, cargs, join(wd, fn), emr))
    pool.close()
    pool.join()


@manager.command
def plot_abnormal_return(wd):
    for fn in glob(join(wd, 'AR-*')):
        print fn
        ar = AbnormalReturns(fn, wd)
        ar.correlation_tables(wd)
        ar.plot_country_mesh(wd)
        ar.plot_mesh(wd)
        ar.plot_slim_mesh(wd)
        ar.plot_box(wd)
        del ar


@manager.command
def mine_reputation(wd, users):
    mr(users, join(wd, 'rep_db'))


@manager.command
def mine_first_posts(wd, posts):
    mfp(posts, join(wd, 'longevity_db'))


@manager.command
def user_group_sizes(db_dir, year, week):
    bins = {}
    year = int(year)
    week = int(week)
    for binner, bfn in binning_strategies(db_dir):
        b = binner()
        b.init_binner(bfn)
        bins[b.strategy] = b

    for binner in bins:
        print "Collecting stats for", binner, year, week
        stats = defaultdict(int)
        b = bins[binner]
        for uid in bins['Reputation'].users:
            bin_ = b.binner(uid, year, week)
            stats[bin_] += 1

        print binner
        print stats


if __name__ == '__main__':
    manager.main()
