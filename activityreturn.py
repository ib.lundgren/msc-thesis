from abnormal_return import AbnormalReturn
from utils import user_history


class ActivityReturn(AbnormalReturn):

    strategy = 'Activity'

    def init_binner(self, afn):
        self.activity = user_history(afn)

    def binner(self, user_id, year, week):
        a = self.activity.get(hash((user_id, year, week)), None)
        if a is None:
            return None
        if a == 0:
            return '0.00'
        if 0 < a <= 0.49:
            return '0.00 - 0.49'
        if 0.49 < a <= 1.00:
            return '0.49 - 1.00'
        if 1.00 < a <= 2.00:
            return '1.00 - 2.00'
        if a > 2.00:
            return 'Over 2.00'
        raise ValueError(a)


if __name__ == "__main__":
    ActivityReturn.run()
