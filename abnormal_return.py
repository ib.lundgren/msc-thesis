from os.path import basename

from mrjob.job import MRJob

from utils import parse_measurement


class AbnormalReturn(MRJob):

    bins = (
        ('p == 0', (0.0, 0.00)),
        ('0 < p < 0.5', (0.01, 0.49)),
        ('0.5 <= p < 1.0', (0.50, 0.99)),
        ('p == 1.0', (1.00, 1.00)),
        ('all', (0.00, 1.00)),
        ('control', (-2.00, -2.00)),
    )

    fn_bins = ('P0', 'P+', 'P-', 'P1', 'PA', 'C')

    windows = ('Before', 'Week', 'Month', '2 Months')

    def configure_options(self):
        super(AbnormalReturn, self).configure_options()
        self.add_file_option('--binner_datafile')
        self.add_passthrough_option('--badges', default=False)
        self.add_passthrough_option('--ge', default=0.0)

    def mapper_init(self):
        print "Initialising binner", basename(self.options.binner_datafile)
        self.pid_type = (lambda x: str(x[1:-1])) if self.options.badges else float
        self.init_binner(basename(self.options.binner_datafile))
        self.ge = self.options.ge
        print "Done initialising", self

    def mapper(self, _, line):
        if len(line) > 120:
            return
        meta, change = parse_measurement(line, self.pid_type)
        for q, score in self.bins:
            bin_ = self.binner(meta[1], meta[4], meta[5])
            if bin_ is None:
                continue
            if score[0] <= meta[3] <= score[1]:
                if change[0] >= self.ge:
                    if q == 'control':
                        for j, window in enumerate(self.windows[1:]):
                            if change[j+1] >= self.ge:
                                for cq, _ in self.bins[:-1]:
                                    yield (cq, window, meta[0], bin_), ([], [change[j+1] - change[0]])
                                    yield (cq, window, 'Global', bin_), ([], [change[j+1] - change[0]])
                    else:
                        for j, window in enumerate(self.windows[1:]):
                            if change[j+1] >= self.ge:
                                yield (q, window, meta[0], bin_), ([change[j+1] - change[0]], [])
                                yield (q, window, 'Global', bin_), ([change[j+1] - change[0]], [])


    def reducer(self, qwcl, subject_controls):
        subjects, controls = [], []
        for sub, con in subject_controls:
            subjects.extend(sub)
            controls.extend(con)
        if len(subjects) > 0 and len(controls) > 0:
            yield qwcl, (subjects, controls)

    def steps(self):
        return [
            self.mr(mapper=self.mapper,
                    mapper_init=self.mapper_init,
                    reducer=self.reducer),
        ]


if __name__ == "__main__":
    AbnormalReturn.run()
