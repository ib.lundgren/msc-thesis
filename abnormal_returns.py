from collections import defaultdict
from functools import partial
from os.path import basename, exists, join

import matplotlib.pyplot as plt
import numpy as np
from numpy import array, percentile, std, median
import prettyplotlib as ppl
from prettyplotlib import brewer2mpl
from scipy.stats import mannwhitneyu, pearsonr

from abnormal_return import AbnormalReturn
from cultural_factors import cultural_factors
from binning import power_distance


def first(x):
    try:
        x, p, _ = x.partition(' ')
        if p:
            return float(x) + 0.001
        else:
            return float(x)
    except AttributeError:
        return first(x[0])
    except ValueError:
        return x


def ev(s):
    subs, cons = s[2:-3].split('], [')
    subs = tuple(float(x) for x in subs.split(','))
    cons = tuple(float(x) for x in cons.split(','))
    return np.array(subs), np.array(cons)


class AbnormalReturns(object):

    bins = [b for b, _ in AbnormalReturn.bins[:-1]]
    windows = AbnormalReturn.windows

    @property
    def countries(self):
        pd_countries = filter(lambda c: c in power_distance, self._countries)
        return sorted(pd_countries, key=lambda c: power_distance[c])

    @property
    def labels(self):
        return sorted(self._labels, key=first)

    def __init__(self, arfn, output):
        self.raw_data = defaultdict(
            partial(defaultdict,
                    partial(defaultdict,
                            partial(defaultdict, list))))
        self.raw_global_data = defaultdict(
                partial(defaultdict,
                        partial(defaultdict, list)))
        self.data = defaultdict(
            partial(defaultdict,
                    partial(defaultdict,
                            partial(defaultdict, list))))
        self.global_data = defaultdict(
                partial(defaultdict,
                        partial(defaultdict, list)))
        self.correlations = defaultdict(
            partial(defaultdict,
                    partial(defaultdict, list)))

        self.fcorrelations = defaultdict(
            partial(defaultdict,
                    partial(defaultdict,
                        partial(defaultdict, str))))

        self._labels = set()
        self._countries = set()

        _, self.metric, self.binner = basename(arfn).split('-')
        fn = join(output, 'G_' + self.metric + "_" + self.binner + '_mesh.png')
        if exists(fn):
            print "Ignoring since it exists", fn
            self._ignore = True
            #return
        else:
            self._ignore = False

        with open(arfn) as f:
            for line in f:
                meta, _, ar = line.partition('\t')
                q, w, c, l = eval(meta)
                ar = ev(ar)
                self._labels.add(l)
                if c == 'Global':
                    self.global_data[q][w][l] = self.calculate_stats(ar)
                    self.raw_global_data[q][w][l] = ar
                else:
                    self._countries.add(c)
                    self.data[q][w][l][c] = self.calculate_stats(ar)
                    self.raw_data[q][w][l][c] = ar

        for q in self.bins:
            for w in self.windows[1:]:
                for b in self._labels:
                    for c in self._countries:
                        if c not in self.data[q][w][b]:
                            self.data[q][w][b][c] = None
                            self.raw_data[q][w][b][c] = None
                    if b not in self.global_data[q][w]:
                        self.global_data[q][w][b] = None
                        self.raw_global_data[q][w][b] = None

        self.calculate_correlations()

    def calculate_correlations(self):
        factors = sorted(cultural_factors.keys())
        for q in self.data:
            for w in self.data[q]:
                for b in self.data[q][w]:
                    for f in cultural_factors:
                        ar = []
                        pwr = []
                        for c, v in self.data[q][w][b].items():
                            if v is not None and c in cultural_factors[f]:
                                ar.append(v[1])
                                pwr.append(cultural_factors[f][c])
                        if ar and pwr:
                            corr, p = pearsonr(ar, pwr)
                            if p < 0.001:
                                self.correlations[q][w][b].append('%d: %.2f!!' % (factors.index(f), corr))
                                self.fcorrelations[f][q][w][b] = '%.2f***' % corr
                            elif p < 0.01:
                                self.correlations[q][w][b].append('%d: %.2f!' % (factors.index(f), corr))
                                self.fcorrelations[f][q][w][b] = '%.2f**' % corr
                            elif p < 0.05:
                                self.correlations[q][w][b].append('%d: %.2f' % (factors.index(f), corr))
                                self.fcorrelations[f][q][w][b] = '%.2f*' % corr
                            elif not np.isnan(corr):
                                # TODO: replace for tables?
                                #self.fcorrelations[f][q][w][b] = '%.2f' % corr
                                self.fcorrelations[f][q][w][b] = '-'
                            else:
                                self.fcorrelations[f][q][w][b] = '-'
                        else:
                            self.fcorrelations[f][q][w][b] = '-'

    abbrs = {
        'p == 0': 'P0',
        '0 < p < 0.5': 'P-',
        '0.5 <= p < 1.0': 'P+',
        'p == 1.0': 'P1',
        'all': 'PA',
    }

    def correlation_tables(self, output):
        fn = join(output, 'F_' + self.metric + "_" + self.binner)
        with open(fn, 'w') as of:
            for f in self.fcorrelations:
                of.write('\n\n' + f + '\n\n')
                of.write('\\begin{table}[!hbp]\n')
                of.write('\\centering\n')
                of.write('\\tiny\n')
                of.write('\\begin{tabular}{rccc' + 'c' * (3 * len(self._labels)) + '}\n')
                of.write('& \\multicolumn{%d}{c}{W1} & & \\multicolumn{%d}{c}{W2} & & \\multicolumn{%d}{c}{W3} \\\\\n' % tuple([len(self.labels)]*3))
                for q in self.bins:
                    row = [self.abbrs[q]]
                    for w in self.windows[1:]:
                        for b in self.labels:
                            if '*' in self.fcorrelations[f][q][w][b]:
                                row.append('\\textbf{' + self.fcorrelations[f][q][w][b] + '}')
                            else:
                                row.append(self.fcorrelations[f][q][w][b])
                        row.append(' ')
                    of.write(' & '.join(row) + '\\\\\n')

                for _ in self.windows[1:]:
                    for l in self.labels:
                        of.write(' & \\rotatebox{270}{%s}' % l)
                    of.write(' & ')
                of.write('\\\\\n')
                of.write('\\end{tabular}\n')
                of.write('\\caption{Correlations between %s and impact on %s for various levels of %s.}\n' % (f.replace('_', ' '), self.metric.lower(), self.binner.lower()))
                of.write('\label{tbl-c-%s-%s-%s}\n' % (self.metric.lower(), self.binner.lower(), f))
                of.write('\\end{table}\n')

    def plot_box(self, output):
        rows = len(self.labels)
        cols = 3
        fig, axes = plt.subplots(rows, cols, sharex=False)

        wins = {
            'Before': 'W0',
            'Week': 'W1',
            'Month': 'W2',
            '2 Months': 'W3'
        }
        factors = {
            'power_distance': 'PD',
            'individualism': 'IND',
            'masculinity': 'MAS',
            'uncertainty_avoidance': 'UA',
            'long_term': 'LT',
            'size_sq_km': 'SQ',
            'population_est_2013': 'POP',
            'mobile_subscriber_million': 'MMS',
            'internet_users_million': 'MIU',
            'GDP_per_capita_in_dollars': 'GDP',
            'world_value_survey': 'WVS',
            'overall_pace_means': 'PoL',
        }
        for i, b in enumerate(self.labels):
            for j, w in enumerate(self.windows[1:]):
                row, ticks = [], []
                controlled = False
                cfactors = []
                for k, q in enumerate(self.bins):
                    if self.raw_global_data[q][w][b] is None:
                        print "Ignoring", q, w, b
                        import numpy as np
                        scrap = np.random.standard_normal(100)
                        self.raw_global_data[q][w][b] = (scrap, scrap)
                        #continue

                    if not controlled:
                        y = self.raw_global_data[q][w][b][1]
                        row.append(y)

                        controlled = True
                        for f in self.fcorrelations:
                            if sum(int(self.fcorrelations[f][qt][w][b] != '-') for qt in self.bins) >= 2:
                                cfactors.append(f)
                            corr = ''.join('\n%s' % factors[cf] for cf in cfactors)

                        tick = 'C\n%.2fk\nMid %.2f\nStd %.2f\n%s' % (len(y) / 1000.0, median(y), std(y), corr)
                        ticks.append(tick)

                    y = self.raw_global_data[q][w][b][0]
                    row.append(y)

                    corr = ''
                    for f in cfactors:
                        corr += '\n%s' % self.fcorrelations[f][q][w][b]

                    tick = '%s\n%.2fk\nMid %.2f\nStd %.2f\n%s' % (self.abbrs[q], len(y) / 1000.0, median(y), std(y), corr)
                    ticks.append(tick)

                if row:
                    ppl.boxplot(axes[i, j], row, xticklabels=ticks)
                axes[i, j].grid(True)
                axes[i, j].set_ylim(-6, 4)
                axes[i, j].set_title('%s user bin %s, Window %s' % (self.binner, b, wins[w]))
                axes[i, j].set_ylabel(self.metric + ' AR')
                axes[i, j].set_xlabel('Users binned by event.')

        fig.set_size_inches(24, 4 * len(self.labels))
        fig.tight_layout()
        fn = join(output, 'B_' + self.metric + "_" + self.binner + '_grid.png')
        print "Storing", fn
        fig.savefig(fn)
        plt.close(fig)

    def _plot_box(self, output):
        if self._ignore:
            return
        rows = len(self.bins)
        cols = 3
        fig, axes = plt.subplots(rows, cols, sharex=False)
        for i, q in enumerate(self.bins):
            for j, w in enumerate(self.windows[1:]):
                row, ticks = [], []
                for b in self.labels:
                    if self.raw_global_data[q][w][b] is None:
                        print "Ignoring", q, w, b
                        continue
                    y = self.raw_global_data[q][w][b][0]
                    row.append(y)
                    tick = 'Subjects\n%s\n%.2fk\nMid %.2f\nStd %.2f' % (b, len(y) / 1000.0, median(y), std(y))
                    ticks.append(tick)

                    y = self.raw_global_data[q][w][b][1]
                    row.append(y)
                    tick = 'Controls\n%s\n%.2fk\nMid %.2f\nStd %.2f' % (b, len(y) / 1000.0, median(y), std(y))
                    ticks.append(tick)

                positions = []
                for a in range(1, len(ticks), 2):
                    positions.append(a - 0.5)
                    positions.append(a + 0.5)

                if row:
                    ppl.boxplot(axes[i, j], row, positions=positions, xticklabels=ticks)
                axes[i, j].grid(True)
                axes[i, j].set_ylim(-5, 5)
                axes[i, j].set_title('Event class %s. Observation window %s.' % (q, w))
                axes[i, j].set_ylabel('Abnormal return in ' + self.metric)
                axes[i, j].set_xlabel('Users binned by %s prior to studied event' % self.binner)

        fig.set_size_inches(32, 24)
        fig.tight_layout()
        fn = join(output, 'B_' + self.metric + "_" + self.binner + '_grid.png')
        print "Storing", fn
        fig.savefig(fn)
        plt.close(fig)

    def plot_mesh(self, output):
        if self._ignore:
            return
        fig, ax = plt.subplots(1)

        self.actual_bins = set()
        data = []
        for q in self.bins:
            row = []
            for w in self.windows[1:]:
                for b in self.labels:
                    row.append(self.global_data[q][w][b])
            if any(row):
                self.actual_bins.add(q)
                data.append(row)
        self.actual_bins = [q for q in self.bins if q in self.actual_bins]
        if len(self.actual_bins) == 2:
            self.actual_bins.pop(0)
            data.pop(0)

        factors = sorted(cultural_factors.keys())
        factors = '\n'.join('%s: %s' % k for k in enumerate(factors))
        ax.annotate('Factors\n' + factors,
                    xy=(0, 0),
                    xytext=(0.5 + 3 * len(self.labels), len(self.actual_bins) / 2.0),
                    va='center',
                    size='small',
                    annotation_clip=False
        )

        for q in self.actual_bins:
            for w in self.windows[1:]:
                for b in self.labels:
                    if self.correlations[q][w][b]:
                        corr = '\n'.join(sorted(self.correlations[q][w][b]))
                        y = self.actual_bins.index(q)
                        x = len(self.labels) * self.windows[1:].index(w) + self.labels.index(b)
                        ax.annotate(corr,
                                    xy=(x,y),
                                    xytext=(x + 0.5, y + 0.95),
                                    alpha=0.7,
                                    size=7,
                                    weight='bold',
                                    ha='center',
                                    va='top'
                        )

        self._plot_mesh(fig, ax, data, 1, -0.3, 'G_', output)

    def plot_slim_mesh(self, output):
       #if self._ignore:
       #    return
        fig, ax = plt.subplots(1)

        self.actual_bins = set()
        data = []
        for q in self.bins:
            row = []
            for w in self.windows[1:]:
                for b in self.labels:
                    row.append(self.global_data[q][w][b])
            if any(row):
                self.actual_bins.add(q)
                data.append(row)
        self.actual_bins = [q for q in self.bins if q in self.actual_bins]
        if len(self.actual_bins) == 2:
            self.actual_bins.pop(0)
            data.pop(0)

        self._plot_mesh(fig, ax, data, 1, -0.3, 'S_', output)

    def plot_country_mesh(self, output):
        if self._ignore:
            return
        fig, ax = plt.subplots(1)

        self.actual_bins = set()
        data = []
        for q in self.bins:
            for c in self.countries:
                row = []
                for w in self.windows[1:]:
                    for b in self.labels:
                        row.append(self.data[q][w][b][c])
                if any(row):
                    self.actual_bins.add(q)
                    data.append(row)

        self.actual_bins = [q for q in self.bins if q in self.actual_bins]
        if len(self.actual_bins) == 2:
            self.actual_bins.pop(0)
            for _ in self.countries:
                data.pop(0)

        self._plot_mesh(fig, ax, data, len(self.countries), 0.0, 'C_', output)

    def _plot_mesh(self, fig, ax, data, step, d, prefix, output):
        l = len(self.labels)

        ax.axvline(l, color='white', linewidth=10)
        ax.axvline(2 * l, color='white', linewidth=10)
        ax.axvline(3 * l, color='white', linewidth=10)

        off = step * len(self.actual_bins)
        ax.annotate("Week", xy=(2, off), xytext=(l / 2.0, off), annotation_clip=False, ha='center')
        ax.annotate("Month", xy=(l + 2, off), xytext=(l + l / 2.0, off), annotation_clip=False, ha='center')
        ax.annotate("2 Months", xy=(2 * l + 2, off), xytext=(2 * l + l / 2.0, off), annotation_clip=False, ha='center')

        for i, b in zip(range(step / 2, step * 5 + step, step), self.actual_bins):
            ax.annotate(b, xy=(-0.25, i + 0.5), xytext=(-0.25, i + 0.5),
                        annotation_clip=False,
                        ha='center',
                        va='center',
                        size='small',
                        rotation='vertical')

        for i in range(1, 4 * l):
            ax.axvline(i, color='white', linewidth=2)
        for i in range(1, step * 5):
            if i % step == 0:
                ax.axhline(i, color='white', linewidth=7)
            else:
                ax.axhline(i, color='white', linewidth=2)

        for y, row in enumerate(data):
            for x, v in enumerate(row):
                if not v:
                    data[y][x] = 0.0
                    ax.annotate('Miss',
                                xy=(x,y),
                                xytext=(x + 0.2, y + 0.4),
                                alpha=0.6,
                                size='small'
                    )
                else:
                    weight = 'bold' if v[-1] else 'normal'
                    diff, smid, cmid, sstd, cstd, ssize, csize, p = v
                    s = tuple(v[1:-3] + (ssize / 1000.0, csize / 1000.0))
                    txt = 'Sub | Con \n%.2f | %.2f\n%.2f | %.2f\n%.2fk\n%.2fk' % s
                    ax.annotate('%.2f' % diff,
                                xy=(x,y),
                                xytext=(x + 0.5, d + y + 0.8),
                                alpha=0.7,
                                size='small',
                                ha='center',
                                weight=weight
                    )
                    ax.annotate(txt,
                                xy=(x,y),
                                xytext=(x + 0.5, y + 0.15),
                                alpha=0.6,
                                size=6,
                                ha='center'
                    )
                    data[y][x] = diff

        data = np.array(data, dtype=np.float)
        m = round(max(data.max(), abs(data.min())) * 1.2, 1)
        green_purple = brewer2mpl.get_map('PiYG', 'diverging', 11).mpl_colormap

        ylbls = (self.countries if not d else ['']) * len(self.actual_bins)
        _, cbar = ppl.pcolormesh(fig, ax, data,
                    cmap=green_purple,
                    vmax=m,
                    vmin=-m,
                    yticklabels=ylbls,
                    xticklabels=self.labels * len(self.windows),
                    xticklabels_rotation=90,
                    center_value=0)

        ax.set_yticklabels(ylbls, size='small')
        ax.set_xticklabels(self.labels * len(self.windows), size='small', rotation=90)
        ax.tick_params(axis='y', pad=30)

        plt.ylabel('Event class, quality p / (p+n)', labelpad=20)
        plt.xlabel('Binning by ' + self.binner, labelpad=20)

        fig.set_size_inches(2 + 3 * l, 8 + len(self.actual_bins) * step)
        fig.tight_layout()
        fn = join(output, prefix + self.metric + "_" + self.binner + '_mesh.png')
        print "Storing", fn
        fig.savefig(fn)
        plt.close(fig)


    def calculate_stats(self, d):
        subjects, controls = d
        subjects = array(subjects)
        controls = array(controls)
        smid = percentile(subjects, 50)
        cmid = percentile(controls, 50)
        sstd = std(subjects)
        cstd = std(controls)
        ssize = len(subjects)
        csize = len(controls)
        try:
            _, p = mannwhitneyu(subjects, controls)
            p = int((p * 2) < 0.01)
        except ValueError:
            p = 0
        return (smid - cmid, smid, cmid, sstd, cstd, ssize, csize, p)
