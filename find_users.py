from collections import defaultdict
from datetime import datetime, timedelta

from lxml import etree

from tag_badges import tag_badges
from utils import to_date, add_yw, load_event_weeks


def load_users_and_posts(pfn, pids):
    users = {}
    for _, elem in etree.iterparse(pfn, events=('start',)):
        if elem.tag == 'row':
            post_id = int(elem.attrib['Id'])
            user_id = int(elem.attrib['OwnerUserId'])
            country = elem.attrib['Country']
            users[post_id] = (country, user_id)
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]

    posts = defaultdict(list)
    with open(pids) as f:
        for line in f:
            pid, _, c = line.partition('\t')
            pid = int(pid)
            created = to_date(c[c.find('"') + 1: c.rfind('"')])
            posts[users[pid]].append((created, pid))
    return users, posts


def filter_events(user_data, future=False, key=None):
    before = timedelta(days=7*8)
    after = timedelta(days=7*8)
    end = datetime(2014, 5, 14, 0, 0, 0) - before
    for user in user_data:
        events = []
        all_events = sorted(user_data[user], reverse=True)
        filtered_events = [cp for cp in all_events if cp[0] <= end]
        later = None

        if not future:
            following_events = [cp for cp in all_events if cp[0] > end]
            if following_events:
                later = following_events[-1][0]

        for i, (c, p) in enumerate(filtered_events):
            looks_ok = True
            earlier = filtered_events[i - 1][0] if i > 0 else None
            if earlier is not None:
                looks_ok &= c - before > earlier
            if not future and later is not None:
                looks_ok &= c + after < later
            if looks_ok:
                events.append((c, p))
                later = c
        events = [(c.isoformat(), p) for c, p in events]
        yield user, events


def find_users(pfn, pids, output, future=False):
    _, posts = load_users_and_posts(pfn, pids)
    with open(output, "w") as f:
        for user, events in filter_events(posts, future=future):
            f.write("%r\t%r\n" % (list(user), events))

    with open(output + "_all", "w") as f:
        for user, events in posts.items():
            events = [(c.isoformat(), p) for c, p in events]
            f.write("%r\t%r\n" % (list(user), events))


def find_badges_users(bfn, output, future=False):
    interesting = [
       #'Critic',
       #'Nice Question',
       #'Nice Answer',
       #'Enlightened',
        'Civic Duty',
        'Good Question',
        'Good Answer',
        'Mortarboard',
        'Guru',
        'Great Answer',
        'Great Question',
    ] + list(tag_badges)
    badges = defaultdict(list)
    sinteresting = set(interesting)
    for _, elem in etree.iterparse(bfn, events=('start',)):
        if elem.tag == 'row':
            name = str(elem.attrib['Name'])
            if name in sinteresting:
                user_id = int(elem.attrib['UserId'])
                created = to_date(elem.attrib['Date'])
                country = elem.attrib['Country']
                badges[(country, user_id)].append((created, name))
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]

    with open(output, "w") as f:
        for user, events in filter_events(badges, future=future):
            f.write("%r\t%r\n" % (list(user), events))

    with open(output + "_all", "w") as f:
        for user, events in badges.items():
            events = [(c.isoformat(), p) for c, p in events]
            f.write("%r\t%r\n" % (list(user), events))

def find_controls(all_subjects, output):
    badges, timeslots = load_event_weeks(all_subjects)
    inv_badges = defaultdict(set)
    for cu, events in badges.items():
        for y, w, _ in events:
            inv_badges[(y, w)].add(cu)

    controls = {}
    for year, week in timeslots:
        event_users = set()
        for d in range(-8, 9):
            y, w = add_yw(year, week, d)
            event_users |= inv_badges[(y, w)]
        controls[(year, week)] = list(event_users)

    with open(output, 'w') as f:
        for yw in controls:
            f.write("%r\t%r\n" % (yw, list(controls[yw])))

