from os.path import basename, dirname, join

from lxml import etree
import pycountry


us = set(['AK', 'AL', 'AR', 'AS', 'AZ', 'CA', 'CO', 'CT', 'DC', 'DE', 'FL', 'GA',
      'GU', 'HI', 'IA', 'ID', 'IL', 'IN', 'KS', 'KY', 'LA', 'MA', 'MD', 'ME',
      'MI', 'MN', 'MO', 'MP', 'MS', 'MT', 'NA', 'NC', 'ND', 'NE', 'NH', 'NJ',
      'NM', 'NV', 'NY', 'OH', 'OK', 'OR', 'PA', 'PR', 'RI', 'SC', 'SD', 'TN',
      'TX', 'UT', 'VA', 'VI', 'VT', 'WA', 'WI', 'WV', 'WY', 'ALASKA', 'ALABAMA',
      'ARKANSAS', 'AMERICAN SAMOA', 'ARIZONA', 'CALIFORNIA', 'COLORADO',
      'CONNECTICUT', 'DISTRICT OF COLUMBIA', 'DELAWARE', 'FLORIDA', 'GEORGIA',
      'GUAM', 'HAWAII', 'IOWA', 'IDAHO', 'ILLINOIS', 'INDIANA', 'KANSAS',
      'KENTUCKY', 'LOUISIANA', 'MASSACHUSETTS', 'MARYLAND', 'MAINE', 'MICHIGAN',
      'MINNESOTA', 'MISSOURI', 'NORTHERN MARIANA ISLANDS', 'MISSISSIPPI',
      'MONTANA', 'NATIONAL', 'NORTH CAROLINA', 'NORTH DAKOTA', 'NEBRASKA',
      'NEW HAMPSHIRE', 'NEW JERSEY', 'NEW MEXICO', 'NEVADA', 'NEW YORK',
      'OHIO', 'OKLAHOMA', 'OREGON', 'PENNSYLVANIA', 'PUERTO RICO',
      'RHODE ISLAND', 'SOUTH CAROLINA', 'SOUTH DAKOTA', 'TENNESSEE', 'TEXAS',
      'UTAH', 'VIRGINIA', 'VIRGIN ISLANDS', 'VERMONT', 'WASHINGTON',
      'WISCONSIN', 'WEST VIRGINIA', 'WYOMING'])

canada = set(['AB', 'BC', 'MB', 'NB', 'NL', 'NT', 'NS', 'NU', 'ON', 'PE', 'QC',
          'SK', 'YT', 'Alberta', 'British Columbia', 'Manitoba',
          'New Brunswick', 'Newfoundland and Labrador',
          'Northwest Territories', 'Nova Scotia', 'Nunavut', 'Ontario',
          'Prince Edward Island', 'Quebec', 'Saskatchewan', 'Yukon'])

uk = set(['UK', 'LONDON', 'ENGLAND', 'WALES', 'SCOTLAND'])

russia = set(['RUSSIA'])

common_misses = {
    'United States': us,
    'United Kingdom': uk,
    'Canada': canada,
    'Russian Federation': russia,
}


def guess_country(loc):
    country = loc
    if ',' in loc:
        _, _, country = loc.rpartition(',')
        country = country.strip()

    for ct, states in common_misses.items():
        if country.upper() in states:
            country = ct

    if 'USA' in country:
        country = "United States"

    if 'Iran' in country:
        country = "Iran"

    try:
        country = pycountry.countries.get(name=country).name
    except KeyError:
        country = None

    return country


def insert_country(pfn, ufn, trim):
    prefix = 'LC' if trim else 'C'

    users = {}
    for _, elem in etree.iterparse(ufn, events=('start',)):
        if elem.tag == 'row':
            loc = elem.attrib.get('Location', None)
            if loc is not None:
                user_id = int(elem.attrib['Id'])
                country = guess_country(loc)
                if country is not None:
                    users[user_id] = country
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]

    with open(join(dirname(pfn), prefix + basename(pfn)), 'w') as f:
        f.write('<?xml version="1.0" encoding="utf-8"?>\n')
        f.write('<posts>\n')
        for _, elem in etree.iterparse(pfn, events=('start',)):
            if elem.tag == 'row':
                user_id = elem.attrib.get('OwnerUserId', None)
                if not user_id:
                    continue

                country = users.get(int(user_id), None)
                if not country:
                    continue

                if trim:
                    del elem.attrib['Body']
                elem.attrib['Country'] = country

                f.write(etree.tostring(elem).strip())
                f.write('\n')
            elem.clear()
            while elem.getprevious() is not None:
                del elem.getparent()[0]
        f.write('</posts>\n')
