from utils import user_history
from abnormal_return import AbnormalReturn


class FeedbackReturn(AbnormalReturn):

    strategy = "Feedback"

    def init_binner(self, ffn):
        self.feedback = user_history(ffn)

    def binner(self, user_id, year, week):
        q = self.feedback.get(hash((user_id, year, week)), None)
        if q is None:
            return None
        if q == 0:
            return '0.00'
        if 0 < q < 0.5:
            return '0.00 - 0.49'
        if 0.5 <= q < 1.00:
            return '0.50 - 0.99'
        if q >= 1.0:
            return '1.00'
        if q == -1:
            return None
        if q == -2:
            return None
        raise ValueError(q)


if __name__ == "__main__":
    FeedbackReturn.run()
