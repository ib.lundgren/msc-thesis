from collections import Counter

from mrjob.job import MRJob

from utils import get_postid_and_vote_type, to_date


class ConcentratedArrivals(MRJob):

    def mapper(self, _, line):
        vote, post_id, _ = get_postid_and_vote_type(line)
        if not vote:
            return

        creation_date = to_date(vote['CreationDate']).date()
        post_creation_date = to_date(vote['PostCreationDate']).date()
        pc = (post_id, vote['PostCreationDate'])
        yield pc, (creation_date - post_creation_date).days

    def reducer(self, pc, days):
        c = Counter(days)
        total = float(sum(c.values()))
        seen = 0
        for k in sorted(c):
            seen += c[k]
            if k <= 7:
                if (seen / total) > 0.9:
                    yield pc
            else:
                return


if __name__ == "__main__":
    ConcentratedArrivals.run()
