Investigating votes and badges on StackOverflow
===============================================

This repository store the logic and results associated with my MSc thesis.
The results can be found in the folder "results" and the logic is
distributed across numerous python files.


Implementation overview
-----------------------

Experiments are run through the manage.py script as shown in the
reproduction instructions. For curious readers, this is the place
to start when wanting to understand how the code works and fit together.

Two other files of interest include measure\_change.py and abnormal\_return.py.
The former is used to compute per post quantity, voting quantity as well as
post quality for all four time windows. The latter to group subjects and
controls together across various binning metrics and compute abnormal returns.


How to reproduce
----------------

First install Python library dependencies using pip

pip install -r requirements.txt

Our study grouped results into two from studying vote based events
and two from badges, where each had one set of results for each
filtering approach.

Below you will find complete instructions for reproducing the
results for vote and badges based events with filtering approach A.

Note that running all experiments may take days.

Make sure you have a data dump unzipped with the xml files located
to the location specified in the "dump" variable.

Similarly, make sure you create a folder for results and set the "wd" variable.

The commands you need to run are all through manage.py and if you run into
trouble please open an issue on Bitbucket or email me.

```
mkdir votes_approach_a
wd=votes_approach_a
dump='may_2014/'

# Vote based events 

# Map user countries into each post record and remove post content to save space.
python manage.py insert_countries "$dump""Posts.xml" "$dump""Users.xml"

# Insert country and post creation time into each vote record.
python manage.py insert_post_data "$dump""LCPosts.xml" "$dump""Votes.xml"

# Find which posts get 90% of their votes within a week.
python manage.py concentrated_arrivals "$dump""CUVotes.xml"

# Remove all votes on posts not receiving 90% of their votes in a week.
python manage.py filter_votes "$wd/concentrated_pid_dates" "$dump""CUVotes.xml"

# Find all significant and insignificant posts
python manage.py votes_per_pid "$wd" "$dump""CUVotes.xml" "$dump""CCUVotes.xml"

# Find all subjects 
# Include --future here for filtering approach B
python manage.py find_subjects "$wd" "$dump""LCPosts.xml"

# Find all controls given the subjects we have 
python manage.py find_controls "$wd"

# Measure change in post quantity and quality as well as feedback 
python manage.py measure_change "$wd" "$dump""LCPosts.xml"

# Measure change in voting behaviour
python manage.py measure_voting "$wd" "$dump""CUVotes.xml"

# Merge results for subjects and controls together
python manage.py merge_measurements "$wd"

# Build a database of user reputation
python manage.py mine_reputation "$wd" "$dump""Users.xml"

# Build a database of user longevity
python manage.py mine_first_posts "$wd" "$dump""LCPosts.xml"

# For abnormal_return you can include --emr if you have setup you MrJob config
# for Amazon Elastic MapReduce

# Abnormal return will be computed with users binned by activity, quality, longevity, reputation and feedback

# Abnormal return in activity
python manage.py abnormal_return "$wd" "activity"

# Abnormal return in quality
python manage.py abnormal_return "$wd" "quality"

# Abnormal return in up and down voting 
python manage.py abnormal_return "$wd" "voting_activity"

# Visualise the results and create correlation tables
python manage.py plot_abnormal_return "$wd"


# Badges

python manage.py insert_user_data "$dump""LCPosts.xml" "$dump""Badges.xml"

# Include --future here for filtering approach B
python manage.py find_subjects --badges "$wd" "$dump""CUBadges.xml"

python manage.py find_controls "$wd"

python manage.py measure_change "$wd" "$dump""LCPosts.xml"

python manage.py measure_voting "$wd" "$dump""CUVotes.xml"

python manage.py merge_measurements "$wd"

# For abnormal_return you can include --emr if you have setup you MrJob config
# for Amazon Elastic MapReduce. 
python manage.py abnormal_return --badges "$wd" "activity"

python manage.py abnormal_return --badges "$wd" "quality"

python manage.py abnormal_return --badges "$wd" "voting_activity"

python manage.py plot_abnormal_return "$wd"
```

If you are interested in reproducing the results using Amazon EMR there are a few
more details worth knowing about that. Please contact me and I'll help you out.
