from os import getcwd
from os.path import basename, join

from mrjob.job import MRJob

from utils import to_day, get_post_and_country
from utils import load_event_weeks, load_quality
from utils import load_controls, within_window


class MeasureChange(MRJob):

    def configure_options(self):
        super(MeasureChange, self).configure_options()
        self.add_file_option('--subjects')
        self.add_file_option('--controls')
        self.add_file_option('--votes')
        self.add_passthrough_option('--control', default=False)

    def mapper_init(self):
        self.window = 8
        self.week = 1
        self.month = 4
        subjects = join(getcwd(), basename(self.options.subjects))
        controls = join(getcwd(), basename(self.options.controls))
        votes = join(getcwd(), basename(self.options.votes))
        self.posts, self.timeslots = load_event_weeks(subjects)
        self.quality, self.quality2, self.feedback = load_quality(votes)
        self.controls = load_controls(controls)

    def mapper(self, _, line):
        post, user_id, country = get_post_and_country(line)
        if not country:
            return

        pid = int(post.get('PostId', post['Id']))
        created = to_day(post.get('CreationDate', post.get('Date', None)))
        y, w = created.year, created.isocalendar()[1]

        if self.options.control:
            for year, week in self.timeslots:
                # For efficiency, we store people that are not controls in controls
                if (country, user_id) not in self.controls[(year, week)]:
                    windows = self.place(year, week, y, w, self.change(pid, post))
                    if windows is not None:
                        yield (country, user_id, -3, -2, year, week), windows
        else:
            for year, week, event_pid in self.posts.get((country, user_id), []):
                # TODO: Do we want to division by badge type here?
                if isinstance(event_pid, str):
                    eq = 1.0
                else:
                    eq = self.quality.get(event_pid, None)
                if eq is not None:
                    windows = self.place(year, week, y, w, self.change(pid, post))
                    if windows is not None:
                        yield (country, user_id, event_pid, eq, year, week), windows

    def reducer(self, cu, windows):
        combined = [0] * 4
        counts = [0] * 4
        for w in windows:
            for i in range(0, 4):
                if w[i] is not None:
                    combined[i] += w[i]
                    counts[i] += 1

        results = [0] * 4
        for i in range(0, 4):
            if counts[i] == 0:
                results[i] = -2
            else:
                results[i] = float(combined[i]) / counts[i]

        yield cu, tuple(results)

    others = None

    def place(self, year, week, y, w, v):
        if within_window(year, week, y, w, -self.window, self.window):
            if within_window(year, week, y, w, -self.window, 0):
                return (v, self.others, self.others, self.others)

            elif within_window(year, week, y, w, 0, self.week):
                return (self.others, v, self.others, self.others)

            elif within_window(year, week, y, w, self.week, self.month):
                return (self.others, self.others, v, self.others)

            elif within_window(year, week, y, w, self.month, self.window):
                return (self.others, self.others, self.others, v)
        return None

    def change(self, pid, post):
        raise NotImplementedError()


class MeasureActivityChange(MeasureChange):

    others = 0

    def change(self, pid, post):
        return 1

    def reducer(self, cu, qualities):
        before, week, month, window = 0, 0, 0, 0
        for q in qualities:
            bq, wq, mq, lq = q[:4]
            before += bq
            week += wq
            month += mq
            window += lq
        yield cu, (before / 8.0, week / 1.0, month / 4.0, window / 4.0)


class MeasureUpVotes(MeasureActivityChange):

    def change(self, pid, post):
        return 1 if post['VoteTypeId'] == '2' else 0


class MeasureDownVotes(MeasureActivityChange):

    def change(self, pid, post):
        return 1 if post['VoteTypeId'] == '3' else 0


class MeasureQualityChange(MeasureChange):

    def change(self, pid, post):
        return self.quality.get(pid, 0.0)


class MeasureQuality2Change(MeasureChange):

    def change(self, pid, post):
        return self.quality2.get(pid, 0.0)


class MeasureFeedbackChange(MeasureChange):

    def change(self, pid, post):
        return self.feedback.get(pid, 0.0)


change_metrics = [
    ('activity', MeasureActivityChange),
    ('quality', MeasureQualityChange),
   #('quality2', MeasureQuality2Change),
    ('feedback', MeasureFeedbackChange),
]

voting_metrics = [
    ('voting_activity', MeasureActivityChange),
    ('upvotes', MeasureUpVotes),
    ('downvotes', MeasureDownVotes),
]
