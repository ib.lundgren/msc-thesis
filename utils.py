from collections import defaultdict
from datetime import datetime, date
import gzip
from random import randint
from time import sleep
from traceback import print_exc


def to_date(d):
    Y = int(d[:4])
    M = int(d[5:7])
    D = int(d[8:10])
    h = int(d[11:13])
    m = int(d[14:16])
    s = int(d[17:19])
    ms = 0
    if len(d) > 19:
        ms = int(d[20:])
    return datetime(Y, M, D, h, m, s, ms)


def to_day(d):
    return date(int(d[:4]), int(d[5:7]), int(d[8:10]))


def parseRow(line):
    if not line.strip().startswith("<row"):
        return None
    attr = {}
    for kv in line[5:-3].split('" '):
        k, _, v = kv.partition("=")
        attr[k] = v[1:]
    return attr



def run_job(MC, cargs, fn, emr=False):
    # Cant pass in job instance because of serialisation when
    # passing between processes.
    print "Storing output in", fn
    try:
        if emr:
            extra = ['--no-output', '--output-dir', fn]
            cargs = ['-r', 'emr'] + extra + cargs
            print "Running on EMR", cargs
        else:
            print "Running locally", cargs
            sleep(randint(0, 99) / 100.0)

        job = MC(cargs)
        print "Running", job
        with job.make_runner() as runner:
            runner.run()
            if not emr:
                with open(fn, 'w') as f:
                    for line in runner.stream_output():
                        f.write(line)
                    f.flush()
    except Exception as e:
        print "Caught exception in worker", MC, cargs, fn
        print "Error:", e
        print_exc()
        print
        raise e


def get_post_and_country(line):
    attrs = parseRow(line)
    if attrs is None:
        return None, None, None
    user_id = int(attrs.get('OwnerUserId', attrs.get('UserId', None)))
    return attrs, user_id, attrs['Country']


def get_postid_and_vote_type(line):
    attrs = parseRow(line)
    if attrs is None:
        return None, None, None
    return attrs, int(attrs['PostId']), int(attrs['VoteTypeId'])


def load_event_weeks(fn):
    posts = defaultdict(list)
    timeslots = set()
    with open(fn) as f:
        for line in f:
            cu, _, dp = line.partition('\t')
            cu = tuple(eval(cu))
            for event in eval(dp):
                d = to_date(event[0])
                week = d.isocalendar()[1]
                posts[cu].append((d.year, week, event[1]))
                timeslots.add((d.year, week))
    return posts, timeslots


def load_controls(fn):
    controls_per_week = defaultdict(set)
    if not fn:
        return controls_per_week

    with open(fn) as f:
        for line in f:
            yw, _, cus = line.partition('\t')
            yw = tuple(eval(yw))
            for cu in eval(cus):
                controls_per_week[yw].add(tuple(cu))
    return controls_per_week


def add_yw(y, w, weeks):
    num_weeks = 53 if y == 2009 else 52
    if ((w + weeks) % num_weeks) == (w + weeks):
        if (w + weeks) == 0:
            if weeks < 0:
                return y - 1, 53 if (y - 1) == 2009 else 52
            else:
                return y + 1, 1
        else:
            return y, w + weeks
    else:
        if (w + weeks) < 0:
            return y - 1, (w + weeks) % num_weeks
        else:
            return y + 1, (w + weeks) % num_weeks


def within_window(year, week, y, w, before, after):
    if year == y and week == w:
        return False
    b = add_yw(year, week, before)
    a = add_yw(year, week, after)
    return b <= (y, w) <= a


def parse_pn(s):
    i = s.find('"') - 2
    if i < 0:
        i = -2
    p, _, n = s[1:i].partition(',')
    return int(p), int(n)


def load_quality(fn):
    quality = {}
    quality2 = {}
    feedback = {}
    with open(fn) as f:
        for line in f:
            pid, _, pn = line.partition('\t')
            pid = int(pid)
            p, n = parse_pn(pn)
            quality[pid] = float(p) / (p + n)
            quality2[pid] = float(p - n) / (p + n)
            feedback[pid] = float((p + n) > 0)
    return quality, quality2, feedback


def parse_measurement(line, pid_type):
    mstr, _, cstr = line.partition('\t')
    ms = mstr[1:-1].split(', ')
    meta = (
        ms[0][1:-1],
        int(ms[1]),
        pid_type(ms[2]),
        float(ms[3]),
        int(ms[4]),
        int(ms[5])
    )
    ns = cstr[1:-1].split(', ')
    changes = (
        float(ns[0]),
        float(ns[1]),
        float(ns[2]),
        float(ns[3])
    )
    return meta, changes


def user_history(fn):
    print "Loading history from", fn
    stats = {}
    f = gzip.open(fn, 'rb')
    for line in f:
        if len(line) > 120:
            continue
        mstr, _, cstr = line.partition('\t')
        ms = mstr[1:-1].split(', ')
        meta = hash((
            int(ms[1]),
            int(ms[4]),
            int(ms[5])
        ))
        ns = cstr[1:-2].split(', ')
        stats[meta] = float(ns[0])
    f.close()
    return stats
